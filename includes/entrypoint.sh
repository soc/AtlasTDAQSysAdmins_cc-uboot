#!/bin/bash
export PATH=$PATH:/src/gcc-arm-10.3-2021.07-x86_64-aarch64-none-linux-gnu/bin:/src/gcc-arm-10.3-2021.07-x86_64-aarch64-none-elf/bin


# download u-boot src
echo "##########################################"
echo "### Downloading the U-Boot source code ###"
echo "##########################################"
git -C /src clone https://source.denx.de/u-boot/u-boot.git
cd ./u-boot/
git checkout $UBOOT_VERSION
cd -

echo "##########################################"
echo "### Creating of pmu object ###"
echo "##########################################"
# download the pm_cfg_obj.c from Xilinx (in the future it can be change to an other one)
git -C /src clone https://github.com/Xilinx/embeddedsw.git
# build the pmu object
/src/u-boot/tools/zynqmp_pm_cfg_obj_convert.py /src/embeddedsw/lib/sw_apps/zynqmp_fsbl/misc/pm_cfg_obj.c /src/u-boot/pmu_obj.bin 

# build the ATF
echo "##########################################"
echo "### Creating of ARM Trusted Firmware  ###"
echo "##########################################"
git -C /src clone https://github.com/Xilinx/arm-trusted-firmware.git
cd /src/arm-trusted-firmware
make CROSS_COMPILE=aarch64-none-elf- PLAT=zynqmp RESET_TO_BL31=1 bl31 
cp /src/arm-trusted-firmware/build/zynqmp/release/bl31.bin /src/u-boot/

cd -

# build the pmufw binary
# this part will be change by the https://github.com/lucaceresoli/zynqmp-pmufw-builder script when it will be fix.
echo "##########################################"
echo "### Creating of pmu firmware ###"
echo "##########################################"
git -C /src clone https://github.com/lucaceresoli/zynqmp-pmufw-binaries.git 
cp /src/zynqmp-pmufw-binaries/bin/pmufw-v2020.1.bin /src/u-boot/pmufw.bin

cd /src/u-boot

echo "##########################################"
echo "### Building U-Boot and U-Boot SPL ###"
echo "##########################################"
export CROSS_COMPILE=aarch64-none-linux-gnu-
export DEVICE_TREE=$DT
make distclean
make mrproper
cp /tmp/uboot.cfg .config
make

cp /src/u-boot/spl/boot.bin /build/
cp /src/u-boot/u-boot.itb /build/

echo "########################################################"
setterm -blink on
echo "###                     DONE                         ###"
setterm -blink off
echo "########################################################"
echo "###  you can find all filea in ./build on your host  ###"
echo "###     if you have any suggestion or comment        ###"
echo "###   please contact me (quentin.duponnois@cern.ch)  ###"
echo "########################################################"
