#!/bin/bash

mkdir build
rm -f build/*
docker image build . -t u-boot_xilinx 
docker run --rm -ti -e UBOOT_VERSION="v2021.10" -e DT="zynqmp-zcu104-revC" -v ./build:/build:Z localhost/u-boot_xilinx:latest