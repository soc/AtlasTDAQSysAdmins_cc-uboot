# cc-uboot

Cross Compiler build-chain to create U-Boot and U-Boot SPL for Xilinx ZynqMP 

## Usage

You can change the U-Boot version and the Device Tree in the run.sh script.

You can modify the include/uboot.cfg.

Please do not change this 2 lines:
'''
CONFIG_PMUFW_INIT_FILE="pmufw.bin"
CONFIG_ZYNQMP_SPL_PM_CFG_OBJ_FILE="pmu_obj.bin"
'''

You can run run.sh, after the end of the script/container, u-boot.itb and boot.bin can be found in the build directory.

You can copy and paste them in your SDcard and add fgpa.bin or download.bit (Optional).

## Contact
If you have any comments or suggestions please let us know.
> atlas-tdaq-sysadmins@cern.ch

## Documentation
How to use it (Xilinx Documentation)
> https://xilinx-wiki.atlassian.net/wiki/spaces/A/pages/18842574/U-Boot+Secondary+Program+Loader
