FROM cern/cs8-base
LABEL version="1.0"
LABEL maintainer="Quentin Duponnois - quentin.duponnois@cern.ch"
LABEL description="This container build u-boot and all u-boot components for the Xilinx ZynqMP"
ENV UBOOT_VERSION="v2021.10"
ENV DT="zynqmp-zcu104-revC"
# Crosstool-ng can't be run as root.
#ARG CTNG_UID=1000
#ARG CTNG_GID=1000
#RUN groupadd -g $CTNG_GID ctng
#RUN useradd -d /home/ctng -m -g $CTNG_GID -u $CTNG_UID -s /bin/bash ctng
# Install all required packages 
RUN yum install -y epel-release
RUN yum install -y bc dtc openssl-devel wget git autoconf gperf bison file flex texinfo help2man gcc-c++ libtool make patch ncurses-devel python3 python3-devel perl-Thread-Queue bzip2 git wget which xz unzip rsync
RUN mkdir /build
RUN mkdir /src
WORKDIR /src
RUN wget https://developer.arm.com/-/media/Files/downloads/gnu-a/10.3-2021.07/binrel/gcc-arm-10.3-2021.07-x86_64-aarch64-none-elf.tar.xz
RUN tar -xvf gcc-arm-10.3-2021.07-x86_64-aarch64-none-elf.tar.xz
RUN rm -f gcc-arm-10.3-2021.07-x86_64-aarch64-none-elf.tar.xz
RUN wget https://developer.arm.com/-/media/Files/downloads/gnu-a/10.3-2021.07/binrel/gcc-arm-10.3-2021.07-x86_64-aarch64-none-linux-gnu.tar.xz
RUN tar -xvf gcc-arm-10.3-2021.07-x86_64-aarch64-none-linux-gnu.tar.xz
RUN rm -f gcc-arm-10.3-2021.07-x86_64-aarch64-none-linux-gnu.tar.xz
VOLUME [ "/build" ]
COPY ./includes/* /tmp/
ENTRYPOINT [ "/bin/bash", "/tmp/entrypoint.sh" ]